LivePose Ubuntu/CUDA container
==============================

This directory holds the Dockerfile to generate the LivePose Ubuntu image, as well as scripts to run the image and work with the image to develop and test new features.

Note that this image has been tested on NVIDIA hardware, more specifically graphic cards built with the Turing architecture (GTX 16xx and RTX 20xx series). Some configurations might work on other hardware, sometimes using the CPU only, but it is not guaranteed.

Also note that the image are available on [hub.docker.io](https://hub.docker.io/u/satmtl) so you do not have to build it to use it. See the [Using the image](#using-the-image) section.


## Prerequisites

Even though it is possible to build this image without anything special apart from having Docker installed, you will need the [NVIDIA container toolkit](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html). On Ubuntu 20.04 all of these can be installed as follows:

```bash
# From the NVIDIA documentation
distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
      && curl -s -L https://nvidia.github.io/libnvidia-container/gpgkey | sudo apt-key add - \
      && curl -s -L https://nvidia.github.io/libnvidia-container/$distribution/libnvidia-container.list | sudo tee /etc/apt/sources.list.d/nvidia-container-toolkit.list

sudo apt update
sudo apt install docker.io nvidia-container-toolkit
```

Note that some distributions already have the `nvidia-container-toolkit` packaged, this is the case for Pop_OS! 20.04 for example. For Pop_OS! 20.04 run:

```bash
sudo apt update
sudo apt install nvidia-container-toolkit
```

After installation Docker needs to be restarted:

```bash
sudo systemctl restart docker
```


## Building the image

To build the image as `satmtl/livepose:latest`, run the following command from this directory:

```bash
docker build -t satmtl/livepose:latest .
```

Note that the image will be built using the latest release available (on the `main` branch). This can be changed by editing the `Dockerfile` and checkout another branch right after the cloning of the LivePose repository.


## Using the image

The image tags from [hub.docker.io](https://hub.docker.io/u/satmtl) follow the releases of LivePose, with the `satmtl/livepose:latest` corresponding to the latest release. The scripts mentionned in the following sub-sections use the latest release.

### Running the image

To run the image, you can use the script `run_livepose.py` which takes a few arguments:

```bash
$ ./run_livepose.py -h
usage: run_livepose.py [-h] [--cameras CAMERAS] [--config CONFIG] [--gui]

Execute LivePose from Docker container

optional arguments:
  -h, --help         show this help message and exit
  --cameras CAMERAS  Path to camera devices, space-separated
  --config CONFIG    Path to the configuration file to use
  --gui              Activate the GUI
  --nvidia           Activate acceleration using NVIDIA GPU, if available
```

In the following steps, we will introduce how to run LivePose with two cameras. 
Note that these steps can be adapted for a single camera, by replacing `/dev/video0,/dev/video2` by `/dev/video0`.

To run LivePose with two cameras (provided that the path are the same in the configuration file given as parameter):

```bash
./run_livepose.py --config /path/to/config.json --cameras /dev/video0,/dev/video2 --gui --nvidia
```

This should run LivePose using the cameras accessible from `/dev/video0` and `/dev/video2`, and show the user interface.


### Using the image for development

We provide a script to run a container with this Docker image in daemon mode, so that it is possible to connect to it and test new features from it. It takes the same parameters as `run_livepose.sh`, and can be ran as follows (for example):

```bash
./dev_livepose.py --cameras /dev/video0,/dev/video2 --gui
```

This will run the container and give access to a terminal. Note that as of yet, the whole container will be deleted upon exiting the terminal so make sure that you save any change you do before that.

Also the command line tools print all the commands that are ran, so you can run them directly and modify the arguments. For example this makes it possible to keep the container alive after quitting it, by removing the `--rm` argument.

Please refer to [Docker reference](https://docs.docker.com/reference/) for more information regarding how to manipulate Docker images and containers.
