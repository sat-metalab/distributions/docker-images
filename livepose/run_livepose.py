#!/usr/bin/env python3

import argparse
import signal
import subprocess


container_name = "livepose"

parser = argparse.ArgumentParser(description="Execute LivePose from Docker container")
parser.add_argument("--cameras", default="/dev/video0", type=str, help="Path to camera devices, comma-separated")
parser.add_argument("--config", default="", type=str, help="Path to the configuration file to use")
parser.add_argument("--gui", action="store_true", help="Show the GUI. Not that this runs Docker in privileged mode")
parser.add_argument("--nvidia", action="store_true", help="Activate acceleration using NVIDIA GPU, if available")
arguments = parser.parse_known_args()[0]


def handle_signals(signum, stack) -> None:
    print("Shutting down the container...")
    subprocess.call(f"docker container stop {container_name}", shell=True)

    if arguments.gui:
        print("= Reactivating X11 client filtering =")
        print("  xhost -")
        subprocess.call("xhost -", shell=True)


signal.signal(signal.SIGINT, handle_signals)
signal.signal(signal.SIGTERM, handle_signals)

cmd_line = f"docker run --name {container_name} --network=host"
if arguments.gui:
    cmd_line = f"{cmd_line} --privileged -e DISPLAY -v /tmp/.X11-unix:/tmp/.X11-unix"

camera_paths = arguments.cameras.split(',')
for camera_path in camera_paths:
    cmd_line = f"{cmd_line} --device={camera_path}:{camera_path}"

if arguments.config:
    cmd_line = f"{cmd_line} -v {arguments.config}:/tmp/livepose_config.json"

if arguments.nvidia:
    cmd_line = f"{cmd_line} --gpus all"

cmd_line = f"{cmd_line} --rm satmtl/livepose:latest"

if not arguments.gui:
    cmd_line = f"{cmd_line} -H"

if arguments.config:
    cmd_line = f"{cmd_line} -c /tmp/livepose_config.json"

#
# Execution
#
if arguments.gui:
    print("Deactivating X11 client filtering:")
    print("  xhost +")
    subprocess.call("xhost +", shell=True)

print("= Executing Docker command line =")
print(f"  {cmd_line}")

subprocess.call(cmd_line, shell=True)
